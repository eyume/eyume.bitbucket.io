let gulp = require('gulp');

let jshint = require('gulp-jshint');
// let less = require('gulp-less');
let cleanCSS = require('gulp-clean-css');
let concat = require('gulp-concat');
let uglify = require('gulp-uglify');
let rename = require('gulp-rename');

gulp.task('lint', () => {
  return gulp.src('js/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

// gulp.task('less', () => {
//   return gulp.src('less/*.less')
//     .pipe(less())
//     .pipe(rename('main.css'))
//     .pipe(gulp.dest('css'))
// });

gulp.task('styles', () => {
  return gulp.src('css/*.css')
  .pipe(concat('all.css'))
  .pipe(cleanCSS())
  .pipe(rename('all.min.css'))
  .pipe(gulp.dest('dist/css'));
});

gulp.task('scripts', () => {
  return gulp.src('js/*.js')
    .pipe(concat('all.js'))
    .pipe(rename('all.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('dist/js'));
});

gulp.task('watch', () => {
  // gulp.watch('less/*.less', gulp.series('less'));
  gulp.watch('css/*.css', gulp.series('styles'));
  gulp.watch('js/*.js', gulp.series('scripts'));
});

gulp.task('default', gulp.series('lint', /* 'less', */ 'styles', 'scripts', 'watch'));
